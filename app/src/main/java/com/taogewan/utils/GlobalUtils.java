package com.taogewan.utils;

import android.util.DisplayMetrics;

import com.taogewan.TGWWApplication;


public class GlobalUtils {

    private static int mScreenWidth;
    private static int mScreenHeight;
    private static float mDensity;

    public static void init() {

        DisplayMetrics dm = TGWWApplication.mContext.getResources().getDisplayMetrics();
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;
        mDensity = dm.density;
    }

    public static int getScreenWidth(){
        return mScreenWidth;
    }

    public static int getScreenHeight(){
        return mScreenHeight;
    }

    /**
     * 单位转换dp转px
     */
    public static int dp2px(int dp) {
        return (int) (dp * mDensity + 0.5f);
    }

    public static int px2dp(int px) {
        return (int) (px / mDensity + 0.5f);
    }
}

















