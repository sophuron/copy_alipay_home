package com.taogewan;

import android.app.Application;
import android.content.Context;

import com.taogewan.utils.GlobalUtils;

public class TGWWApplication extends Application {
    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        this.mContext = this;
        GlobalUtils.init();
    }
}
