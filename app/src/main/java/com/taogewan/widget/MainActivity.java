package com.taogewan.widget;

import android.app.Application;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private View parallax;
    private ImageView iv_image;
    private NestedScrollView nsv_scroll;
    private LinearLayout ll_offset;
    private TextView tv_refresh;

    private float mDensity;
    private int mOffset = 0;
    private int mScrollY = 0;
    private float y1 = 0;// 手指按下时的位置
    private float y2 = 0;// 手指偏移时的位置

    private boolean pull_to_refresh = false;

    private float position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DisplayMetrics dm = getResources().getDisplayMetrics();
        mDensity = dm.density;

        parallax = findViewById(R.id.parallax);
        iv_image = findViewById(R.id.iv_image);
        nsv_scroll = findViewById(R.id.nsv_scroll);
        ll_offset = findViewById(R.id.ll_offset);
        position = ll_offset.getTranslationY();
        tv_refresh = findViewById(R.id.tv_refresh);
        nsv_scroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            private int lastScrollY = 0;
            private int h = dp2px(300);
            private int hMinParallax = dp2px(70);
            private int hScrollParallax = h - hMinParallax;
            private int hMaxIv = dp2px(300);// 图片最大高度
            private int hMinIv = dp2px(100);// 图片最小高度
            private int hScrollIv = hMaxIv - hMinIv;//
            private int hScrollYIv = dp2px(20);//图片偏移量
            private int HALF_HEIGHT_BANNER = h / 2;
            private int color = ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)&0x00ffffff;

            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                Log.d("onScrollChange", "scrollX:" +scrollX + " scrollY:" +scrollY + " oldScrollX:" +oldScrollX + " oldScrollY:" +oldScrollY);
                if (lastScrollY < hScrollParallax) {
                    scrollY = Math.min(hScrollParallax, scrollY);
                    mScrollY = scrollY > hScrollParallax ? hScrollParallax : scrollY;
                    parallax.setTranslationY(mOffset - mScrollY);
                }

                if(scrollY <= hScrollParallax) {
                    parallax.setTranslationY(mOffset - mScrollY);
                    double f = (double) scrollY / hScrollParallax;
                    ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) iv_image.getLayoutParams();
                    params.height = hMaxIv - (int)(hScrollIv * f);
                    params.width = params.height;
                    iv_image.setLayoutParams(params);
                    iv_image.setTranslationY((int)(hScrollYIv * f));
                }

                lastScrollY = scrollY;
            }
        });

        nsv_scroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //继承了Activity的onTouchEvent方法，直接监听点击事件
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    //当手指按下的时候
                    y1 = event.getY();
                }
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    //当手指离开的时候
                    y2 = event.getY();
//                        Log.d("ACTION_UP", "y2:" + y2 + " - y1:" + y1 + (y2 - y1));
                    if(pull_to_refresh) {
                        tv_refresh.setText("刷新中...");
                        ll_offset.setTranslationY(position + dp2px(60));
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                //do something
                                ll_offset.setTranslationY(position);
                                tv_refresh.setText("兄弟 你发现我了");
                                pull_to_refresh = false;
                            }
                        }, 3000);    //延时0.1s执行
                    }else {
                        if(ll_offset.getTranslationY() > position) {
                            ll_offset.setTranslationY(position);
                        }
                    }

                }
                if(event.getAction() == MotionEvent.ACTION_MOVE) {
                    //当手指离开的时候
                    y2 = event.getY();
                    if(y2 - y1 > 0 && nsv_scroll.getScrollY() == 0) {
                        if(px2dp((int)((y2 - y1) / 5)) > 60) {
                            pull_to_refresh = true;
                        }else {
                            pull_to_refresh = false;
                        }
                        float py = y2 - y1;
                        ll_offset.setTranslationY(py / 5);
                        return true;
                    }

                }
                return false;
            }
        });

    }

    /**
     * 单位转换dp转px
     */
    public int dp2px(int dp) {
        return (int) (dp * mDensity + 0.5f);
    }

    public int px2dp(int px) {
        return (int) (px / mDensity + 0.5f);
    }

}
